import random

from flask import jsonify
from flask_jwt_extended import get_jwt_identity

from models import User, Transaction

STATUS_OK = 200


def custom_response(code=200, message="SUCCESS", payload={}):
    if code != STATUS_OK:
        return jsonify({"message": message}, code)

    response_data = {
        "status": "SUCCESS",
        "result": payload
    }
    return jsonify(response_data, code)


def get_user():
    identity = get_jwt_identity()
    return User.objects.get(id=identity["_id"])


def get_random_payment():
    payment = [Transaction.DEBIT, Transaction.CREDIT]
    return payment[random.randrange(0, 1)]


def create_transactions(user, amount, _type, payment_type, remarks=None, target_user=None):
    if _type in [Transaction.PAYMENT, Transaction.TRANSFER]:
        balance_before = user.balance
        user.balance -= amount
    else:
        balance_before = user.balance
        user.balance += amount

    transaction = Transaction(
        user=user, amount=amount, balance_before=balance_before, balance_after=user.balance,
        type=_type, payment_type=payment_type, status=Transaction.SUCCESS, remarks=remarks,
        target_user=target_user
    ).save()
    user.save()

    return transaction
