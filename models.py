import datetime
import uuid

from mongoengine import *


class User(Document):
    id = StringField(default=lambda: str(uuid.uuid4()), primary_key=True)
    first_name = StringField(required=True)
    last_name = StringField(required=True)
    phone_number = StringField(required=True, unique=True)
    address = StringField(max_length=125)
    pin = StringField(required=True, max_length=6)
    balance = IntField(default=0)
    updated_date = DateTimeField(default=datetime.datetime.utcnow)
    created_date = DateTimeField(default=datetime.datetime.utcnow)

    def __str__(self):
        return str(self.id)

    def to_dict(self):
        return {
            "user_id": self.id,
            "first_name": self.first_name,
            "last_name": self.last_name,
            "phone_number": self.phone_number,
            "address": self.address,
            "updated_date": self.updated_date.strftime("%Y-%m-%d %H:%M:%S"),
            "created_date": self.created_date.strftime("%Y-%m-%d %H:%M:%S")
        }


class Transaction(Document):
    TOP_UP, PAYMENT, TRANSFER, RECEIVE_TRANSFER = "TOP_UP", "PAYMENT", "TRANSFER", "RECEIVE_TRANSFER"
    TYPE_CHOICES = {
        TOP_UP: "Top Up",
        PAYMENT: "Payment",
        TRANSFER: "Transfer",
        RECEIVE_TRANSFER: "Receive Transfer"
    }

    DEBIT, CREDIT = "DEBIT", "CREDIT"
    PAYMENT_CHOICES = {
        DEBIT: "Debit",
        CREDIT: "Credit"
    }

    SUCCESS, PENDING, ERROR = "SUCCESS", "PENDING", "ERROR"
    STATUS_CHOICES = {
        SUCCESS: "Success",
        PENDING: "Pending",
        ERROR: "Error"
    }

    id = StringField(default=lambda: str(uuid.uuid4()), primary_key=True)
    user = ReferenceField(User)
    target_user = ReferenceField(User, null=True)
    amount = IntField()
    balance_before = IntField()
    balance_after = IntField()
    remarks = StringField(null=True)
    type = StringField(choices=TYPE_CHOICES)
    payment_type = StringField(choices=PAYMENT_CHOICES)
    status = StringField(choices=STATUS_CHOICES)
    created_date = DateTimeField(default=datetime.datetime.utcnow)

    def __str__(self):
        return str(self.id)

    def serializer(self):
        response = {
            f"{self.type.lower()}_id": self.user.id,
            f"ammout": self.amount,
            "balance_before": self.balance_before,
            "balance_after": self.balance_after,
            "created_date": self.created_date.strftime("%Y-%m-%d %H:%M:%S")
        }

        if self.payment_type in [self.PAYMENT, self.TRANSFER]:
            response["remarks"] = self.remarks

        return response

    def full_serializer(self):
        return {
            f"{self.type}_id": self.user.id,
            "status": self.status,
            "user_id": self.user.id,
            "transaction_type": self.payment_type,
            "amount": self.amount,
            "remarks": self.remarks if self.remarks else "",
            "balance_before": self.balance_before,
            "balance_after": self.balance_after,
            "created_date": self.created_date.strftime("%Y-%m-%d %H:%M:%S")
        }
