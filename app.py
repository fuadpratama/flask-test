import datetime

from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    jwt_required,
    get_jwt_identity
)

from helper import get_user, get_random_payment, create_transactions
from tasks.celery import transfer_to_user
from settings import *
from flask import request, jsonify, session

from models import User, Transaction

HTTP_OK = 200
HTTP_BAD_REQUEST = 400
SUCCESS = "SUCCESS"

# We are using the `refresh=True` options in jwt_required to only allow
# refresh tokens to access this route.
@app.route("/refresh", methods=["POST"])
@jwt_required(refresh=True)
def refresh():
    identity = get_jwt_identity()
    access_token = create_access_token(identity=identity)
    refresh_token = create_refresh_token(identity=identity)
    result = {
        "access_token": access_token,
        "refresh_token": refresh_token,
    }

    return jsonify(status=SUCCESS, result=result)


@app.route("/protected")
@jwt_required()
def protected():
    return jsonify(message="you are protected")


@app.route("/register", methods=["POST"])
def register():
    request_data = request.get_json()
    try:
        user = User.objects(phone_number=request_data["phone_number"])
        if user:
            return jsonify({"message": "Phone Number already registered"}), HTTP_BAD_REQUEST

        data = {
            "first_name": request_data["first_name"],
            "last_name": request_data["last_name"],
            "phone_number": request_data["phone_number"],
            "address": request_data["address"],
            "pin": request_data["pin"],
        }

        user = User(**data).save()
    except KeyError as e:
        return jsonify({"message": f"{e.args[0]} is required"}), HTTP_BAD_REQUEST

    return jsonify(status=SUCCESS, result=user.to_dict())


@app.route("/login", methods=["POST"])
def login():
    request_data = request.get_json()
    user = User.objects(phone_number=request_data.get("phone_number")).first()
    if user:
        if user.pin == request_data["pin"]:
            session["logged_in"] = True
            access_token = create_access_token(identity=user)
            refresh_token = create_refresh_token(identity=user)
            response_data = {
                    "access_token": access_token,
                    "refresh_token": refresh_token
                }
            return jsonify(status=SUCCESS, result=response_data)
        else:
            return jsonify({"message": "Invalid Pin"}), HTTP_BAD_REQUEST
    else:
        return jsonify({"message": "user already not exits"}), HTTP_BAD_REQUEST


@app.route("/profile", methods=["POST"])
@jwt_required()
def profile():
    try:
        user = get_user()
    except User.DoesNotExist:
        return jsonify({"message": "User Cannot Exits"}), HTTP_BAD_REQUEST

    user.first_name = request.json.get("first_name") if request.json.get("first_name", None) else user.first_name
    user.last_name = request.json.get("last_name") if request.json.get("last_name", None) else user.last_name
    user.address = request.json.get("address") if request.json.get("address", None) else user.address
    user.pin = request.json.get("pin") if request.json.get("pin", None) else user.pin
    if len(request.json.keys()) >= 1:
        user.updated_date = datetime.datetime.utcnow()
    user.save()

    return jsonify(status=SUCCESS, result=user.to_dict())


@app.route("/topup", methods=["POST"])
@jwt_required()
def topup():
    try:
        user = get_user()
        amount = request.json["amount"]
        payment_type = get_random_payment()
    except User.DoesNotExist:
        return jsonify({"message": "User Cannot Exits"}), HTTP_BAD_REQUEST
    except KeyError as e:
        return jsonify({"message": f"{e.args[0]} is required"}), HTTP_BAD_REQUEST

    transaction = create_transactions(user, amount, Transaction.TOP_UP, payment_type)

    return jsonify(message=SUCCESS, result=transaction.serializer())


@app.route("/pay", methods=["POST"])
@jwt_required()
def pay():
    try:
        user = get_user()
        amount = request.json["amount"]
        remarks = request.json["remarks"]
        payment_type = get_random_payment()
    except User.DoesNotExist:
        return jsonify({"message": "User Cannot Exits"}), HTTP_BAD_REQUEST
    except KeyError as e:
        return jsonify({"message": f"{e.args[0]} is required"}), HTTP_BAD_REQUEST

    if user.balance < amount:
        return jsonify({"message": "your balance is not enough"})

    transaction = create_transactions(user, amount, Transaction.PAYMENT, payment_type, remarks=remarks)

    return jsonify(message=SUCCESS, result=transaction.serializer())


@app.route("/transfer", methods=["POST"])
@jwt_required()
def transfer():
    try:
        user = get_user()
        target_user = User.objects.get(id=request.json["target_user"])
        amount = request.json["amount"]
        remarks = request.json["remarks"]
        payment_type = get_random_payment()
    except User.DoesNotExist:
        return jsonify({"message": "User Cannot Exits"}, HTTP_BAD_REQUEST)
    except KeyError as e:
        return jsonify({"message": f"{e.args[0]} is required"}, HTTP_BAD_REQUEST)

    if user.balance < amount:
        return jsonify({"message": "your balance is not enough"})

    transaction = create_transactions(
        user, amount, Transaction.TRANSFER, payment_type, remarks=remarks, target_user=target_user)
    transfer_to_user.delay(
        request.json["target_user"], amount, Transaction.RECEIVE_TRANSFER, payment_type
    )

    return jsonify(message=SUCCESS, result=transaction.serializer())


@app.route("/transactions")
@jwt_required()
def transactions():
    try:
        user = get_user()
    except User.DoesNotExist:
        return jsonify({"message": "User Cannot Exits"}, HTTP_BAD_REQUEST)

    data_transactions = Transaction.objects(user=user).order_by('-created_date')
    response_data = []
    for transaction in data_transactions:
        response_data.append(transaction.full_serializer())

    return jsonify(message=SUCCESS, result=response_data)


if __name__ == '__main__':
    app.run(debug=True)
