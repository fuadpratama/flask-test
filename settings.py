from datetime import timedelta
from flask import Flask
from flask_jwt_extended import JWTManager
from flask_mongoengine import MongoEngine

app = Flask(__name__)
app.config['SECRET_KEY'] = "0705d5e7-b286-49b7-9ffd-a01fde389a19"
app.config['MONGODB_SETTINGS'] = {
    "db": "test_flask",
}

app.config["JWT_SECRET_KEY"] = "223c6f2b-0049-45e3-b61d-aa9b7fc90046"
app.config["JWT_ACCESS_TOKEN_EXPIRES"] = timedelta(days=1)
app.config["JWT_REFRESH_TOKEN_EXPIRES"] = timedelta(days=30)

db = MongoEngine(app)
jwt = JWTManager(app)
